for version in $(cat .versions);
do
  cd $version
  docker build . -t flovilmart/swift-ci:$version
  docker push flovilmart/swift-ci:$version
  cd ..
done;