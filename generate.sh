for version in $(cat .versions);
do
 mkdir -p $version;
 cat <<EOF > $version/Dockerfile
FROM swift:$version

RUN git clone https://github.com/yonaskolb/Mint.git /tmp/Mint
WORKDIR /tmp/Mint
RUN swift run mint install yonaskolb/mint

RUN mint install realm/SwiftLint 

EOF

done;